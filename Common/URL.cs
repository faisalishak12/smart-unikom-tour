﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unikom_Apps.Common
{
    class URL
    {
        public const string photo360 = "ms-appx:///images/360/";
        public const string tumbnail = "ms-appx:///images/Tumbnail/";
        public const string urlBase = "http://www.unikom-app.net/";
    }
}
