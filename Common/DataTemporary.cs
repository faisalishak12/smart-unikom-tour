﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unikom_Apps.Common
{
    class DataTemporary
    {
        //CategoryTemporary
        public static string imageCategory;
        public static string idCategory;
        public static string category;
        public static int idBuilding;

        //GridView360degree
        public static int count;

        //StandbyScreen
        public static int timeLimit = 1; // In Minute 
        public static int timer1;
        public static int timer2;
        public static int timer3;
        public static int timer4;
        public static int timer5;
        public static int timer6;
        public static int timer7;
        public static int timer8;
        public static bool isStopRunAsyncForTask1; //Used in VDetailFloor   
        public static bool isStopRunAsyncForTask2; //Used in V360Degree 
        public static bool isStopRunAsyncForTask3; //Used in Home
        public static bool isStopRunAsyncForTask4; //Used in StanbyScreen Animation
        public static bool isStopRunAsyncForTask5; //Used in VFloorsRepository
        public static bool isStopRunAsyncForTask6; //Used in VDetailFloor360Degree
        public static bool isStopRunAsyncForTask7; //Used in VGetUnikomApps
        public static bool isStopRunAsyncForTask8; //Used in VGedungOption
        public static bool isNull;
        public static int SizeColectionEvent;
        public static int idArticle;
        public static bool isActive;
    }
}
