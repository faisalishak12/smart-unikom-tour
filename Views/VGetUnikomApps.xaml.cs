﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Unikom_Apps.Common;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.UI.Composition;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Hosting;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class VGetUnikomApps : Page
    {
        public VGetUnikomApps()
        {
            this.InitializeComponent();

            this.Loaded += TimerForStandbyScreen;
            this.Loaded += MainPage_Loaded;
        }

        private async void TimerForStandbyScreen(object sender, RoutedEventArgs e)
        {
            DataTemporary.timer7 = 0;
            DataTemporary.isStopRunAsyncForTask7 = false;
            while (!DataTemporary.isStopRunAsyncForTask7)
            {
                DataTemporary.timer7++;
                await System.Threading.Tasks.Task.Delay(60000);
                Debug.WriteLine("VGetUnikomApps : "+ DataTemporary.timer7);
                if (DataTemporary.timer7 > DataTemporary.timeLimit)
                {
                    DataTemporary.isStopRunAsyncForTask7 = true;
                    DataTemporary.timer7 = 0;
                    Frame.Navigate(typeof(StandbyScreen));
                }
            }
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (ApiInformation.IsApiContractPresent("Windows.Foundation.UniversalApiContract", 2))
            {
                // Get a referece to a "propertyset" that contains the following keys
                //  Translation (Vector3)
                //  CenterPoint (Vector3)
                //  Scale (Vector3)
                //  Matrix (Matrix4x4)
                // that represent the state of the scrollview at any moment (i.e. as the user manipulates the scrollviewer with mouse, touch, touchpad)

                CompositionPropertySet scrollerManipProps = ElementCompositionPreview.GetScrollViewerManipulationPropertySet(Scroll_viewer);

                Compositor compositor = scrollerManipProps.Compositor;

                // Create the expression
                
                ExpressionAnimation expressionOffset = compositor.CreateExpressionAnimation("(scroller.Translation.Y) * parallaxFactorOffset");
                ExpressionAnimation expression = compositor.CreateExpressionAnimation("scroller.Translation.Y * parallaxFactor");

                // wire the ParallaxMultiplier constant into the expression
                expression.SetScalarParameter("parallaxFactor", 0.003f);
                expressionOffset.SetScalarParameter("parallaxFactorOffset", -0.3f);

                // set "dynamic" reference parameter that will be used to evaluate the current position of the scrollbar every frame
                expression.SetReferenceParameter("scroller", scrollerManipProps);
                expressionOffset.SetReferenceParameter("scroller", scrollerManipProps);

                // Get the background image and start animating it's offset using the expression
                Visual backgroundVisualAndroid = ElementCompositionPreview.GetElementVisual(Barcode_download_android);
                backgroundVisualAndroid.StartAnimation("Offset.X", expressionOffset);
                backgroundVisualAndroid.StartAnimation("Scale.Y", expression);
                backgroundVisualAndroid.StartAnimation("Scale.X", expression);

                // Get the background image and start animating it's offset using the expression
              /*  Visual backgroundVisualApple = ElementCompositionPreview.GetElementVisual(Barcode_download_apple);
                backgroundVisualApple.StartAnimation("Offset.X", expressionOffset);
                backgroundVisualApple.StartAnimation("Scale.Y", expression);
                backgroundVisualApple.StartAnimation("Scale.X", expression);*/
                //Visual bg = ElementCompositionPreview.GetElementVisual(Barcode_download);
                //bg.StartAnimation("Offset.X", expression);
                // 
            }
        }

        private void Button_back_Click(object sender, RoutedEventArgs e)
        {
            DataTemporary.isStopRunAsyncForTask7 = true;
            DataTemporary.timer7 = 0;
            Frame.Navigate(typeof(Home));
        }
    }
}
