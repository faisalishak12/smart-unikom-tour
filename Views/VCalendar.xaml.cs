﻿using Chilkat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Unikom_Apps.Common;
using Unikom_Apps.Models;
using Unikom_Apps.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Microsoft.Toolkit.Uwp.UI.Animations;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class VCalendar : Page
    {
        public VCalendar()
        {
            this.InitializeComponent();

            ProfideListview.Blur(
                value: 5,
                duration: 300,
                delay: 0).Start();

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            ObservableCollection<VMCalendar> request = e.Parameter as ObservableCollection<VMCalendar>;

            /*var result = await VMCalendar.LoadURL();
            ObservableCollection<VMCalendar> collectionCalendar = new ObservableCollection<VMCalendar>(result);            

            Debug.WriteLine("size"+collectionCalendar.Count);*/

            /*var task = await VMCalendar.GetCalendarGrouped();

            ObservableCollection<ListGroup> taskResult = new ObservableCollection<ListGroup>(task);

            CalendarCVS.Source = taskResult;

            Debug.WriteLine(taskResult.Count);

            foreach (var msg in taskResult)
            {
                Debug.WriteLine("jaja"+msg.Key);
            }
            */
            //var task =  await );
            //ObservableCollection<ListGroup> taskResult = new ObservableCollection<ListGroup>(task);
            ContactsCVS.Source = VMCalendar.GetContactsGrouped(request);
        }

        private void Button_back_Click(object sender, RoutedEventArgs e)
        {
            DataTemporary.isStopRunAsyncForTask7 = true;
            DataTemporary.timer7 = 0;
            Frame.Navigate(typeof(Home));
        }
    }
}
