﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Brushes;
using Microsoft.Graphics.Canvas.Effects;
using System.Threading.Tasks;
using Unikom_Apps.ViewModels;
using Unikom_Apps.Common;
using Unikom_Apps.Models;
using System.Diagnostics;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class VDetailFloor360Degree : Page
    { 

        public VDetailFloor360Degree()
        {
            this.InitializeComponent();

            this.Loaded += TimerForStandbyScreen;
        }

        private async void TimerForStandbyScreen(object sender, RoutedEventArgs e)
        {
            DataTemporary.isStopRunAsyncForTask6 = false;
            while (!DataTemporary.isStopRunAsyncForTask6)
            {
                DataTemporary.timer6++;
                await System.Threading.Tasks.Task.Delay(60000);
                //debug
                Debug.WriteLine("VDetailFloor360Degree :" + DataTemporary.timer6);

                if (DataTemporary.timer6 > DataTemporary.timeLimit)
                {
                    DataTemporary.isStopRunAsyncForTask6 = true;
                    DataTemporary.timer6 = 0;
                    Frame.Navigate(typeof(StandbyScreen));
                }
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            DataTemporary.idCategory = e.Parameter as string;

            //Hook view model gridview
            VM360Degree vm360Degree = new VM360Degree(DataTemporary.idCategory);
            Gv_360Gegree.DataContext = vm360Degree;

            //Hook view model rotarortile
            //VM360Degree collection = new VM360Degree(DataTemporary.idCategory);
            //Rotator_header.DataContext = collection;
            //Set header content
            Header_description.Text = DataTemporary.category.ToUpper();
        }

        private void Gv_360Gegree_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //TimerStanbySreen
            DataTemporary.timer6 = 0;
            DataTemporary.isStopRunAsyncForTask6 = true;

            //Navigate Page
            M360Degree data = (sender as GridView).SelectedItem as M360Degree; 
            Frame rootFrame = Window.Current.Content as Frame;
            Debug.WriteLine("Data yang dikirim : " + data.imageName);
            rootFrame.Navigate(typeof(V360Degree), data);
        }

        private void Gv_360Gegree_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var panel = (ItemsWrapGrid)Gv_360Gegree.ItemsPanelRoot;

            
            /*if (DataTemporary.count <= 6)
            {
                panel.ItemWidth = e.NewSize.Width / 2;
                panel.ItemHeight = 350;
            }
            else
            {*/

                panel.ItemWidth = e.NewSize.Width / 4;
                panel.ItemHeight = 250;
            

        }

        private void Rotator_tile1_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //Rotator_header.Width = HeaderGrid.Width;
            //Rotator_header.Height= 250;
        }

        private void CommandBar_Tapped(object sender, TappedRoutedEventArgs e)
        {

        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            //end task
            DataTemporary.timer6 = 0;
            DataTemporary.isStopRunAsyncForTask6 = true;
            if (DataTemporary.idCategory == "17") {
                Frame.Navigate(typeof(VGedungOption));
            }
            else
            {
                Frame.Navigate(typeof(MenuUtama), DataTemporary.idBuilding.ToString());
            }
        }
    }
}
