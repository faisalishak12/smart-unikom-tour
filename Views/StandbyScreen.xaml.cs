﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Microsoft.Toolkit.Uwp.UI.Animations;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Unikom_Apps.Common;
using Unikom_Apps.ViewModels;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class StandbyScreen : Page
    {
        public StandbyScreen()
        {
            this.InitializeComponent();

            DataTemporary.isNull = false;
            VMEvent vmHome = new VMEvent();
            CarouselControl.DataContext = vmHome;

            if (!IsInternet())
            {
                BlackContras.Visibility = Visibility.Collapsed;
                TitleEvent.Visibility = Visibility.Collapsed;
                TimeBottom.Visibility = Visibility.Collapsed;
                CarouselControl.Visibility = Visibility.Collapsed;
                LoadAnimation();
            }
            else {
                LoadSlideShow(6);
            }

            Background.Blur(
                   value: 5,
                   duration: 1000,
                   delay: 0).Start();
        }

        private async void LoadSlideShow(int count)
        {
            int i = 0; 
            DataTemporary.isStopRunAsyncForTask4 = false;
            while (!DataTemporary.isStopRunAsyncForTask4)
            {
                _Time.Text = DateTime.Now.ToString("HH:mm");
                _Date.Text = DateTime.Now.ToString("dd-MMMM-yyyy");             

                await System.Threading.Tasks.Task.Delay(3500);
                CarouselControl.SelectedIndex = i;

                i++;

                if (i > count)
                    i = 0; 
            }
        }

        public bool IsInternet()
        {
            ConnectionProfile connections = NetworkInformation.GetInternetConnectionProfile();
            bool internet = connections != null && connections.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
            return internet;
        }

        private async void LoadAnimation()
        {
            DataTemporary.isStopRunAsyncForTask4 = false;
            while (!DataTemporary.isStopRunAsyncForTask4)
            {
                Time.Text = DateTime.Now.ToString("HH:mm");
                Date.Text = DateTime.Now.ToString("dd-MMMM-yyyy");
                Time_state.Text = ConvertToTimeState(DateTime.Now);

                await System.Threading.Tasks.Task.Delay(2500);

                Panel_time.Scale(
                    centerX: 0f,
                    centerY: 0f,
                    scaleX: 0.9f,
                    scaleY: 0.9f,
                    duration: 1000,
                    delay: 0).Start();

                Panel_time.Fade(
                    value: 0.0f,
                    duration: 1000,
                    delay: 0).Start();

                Background.Scale(
                   centerX: 0f,
                   centerY: 0f,
                   scaleX: 1f,
                   scaleY: 1f,
                   duration: 1000,
                   delay: 0).Start();

                try
                {
                    Background.Blur(
                       value: 0,
                       duration: 1000,
                       delay: 0).Start();
                }
                catch
                {

                }

                Panel_tap_here.Offset(
                   offsetX: 0f,
                   offsetY: -700f,
                   duration: 800,
                   delay: 0).Start();

                await System.Threading.Tasks.Task.Delay(2500);

                Panel_time.Fade(
                  value: 1.0f,
                  duration: 1000,
                  delay: 0).Start();

                Panel_time.Scale(
                    centerX: 0f,
                    centerY: 0f,
                    scaleX: 1f,
                    scaleY: 1f,
                    duration: 1000,
                    delay: 0).Start();

                Background.Scale(
                    centerX: 0f,
                    centerY: 0f,
                    scaleX: 1.05f,
                    scaleY: 1.05f,
                    duration: 1000,
                    delay: 0).Start();

                try
                {
                    Background.Blur(
                       value: 6,
                       duration: 1000,
                       delay: 0).Start();
                }
                catch
                {

                }


                Panel_tap_here.Offset(
                    offsetX: 0f,
                    offsetY: 700f,
                    duration: 800,
                    delay: 0).Start();

            }
        }

        private string ConvertToTimeState(DateTime dt)
        {
            if ((dt.Hour >= 00) && (dt.Hour <= 10))
            {
                return "Good Morning";
            }
            else if ((dt.Hour >= 11) && (dt.Hour <= 14))
            {
                return "Good Day";
            }
            else if ((dt.Hour >= 15) && (dt.Hour <= 17))
            {
                return "Good Afternoon";
            }
            else if ((dt.Hour >= 18) && (dt.Hour <= 20))
            {
                return "Good Evening";
            }
            else
            {
                return "Good Night";
            }
        }

        private void Screen_Tapped(object sender, TappedRoutedEventArgs e)
        {
            DataTemporary.isStopRunAsyncForTask4 = true;
            Frame.Navigate(typeof(Home));
        }
    }
}
