﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Unikom_Apps.Common;
using Unikom_Apps.Models;
using Unikom_Apps.ViewModels;
using Unikom_Apps.Views;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MenuUtama : Page
    {
        public MenuUtama()
        {
            this.InitializeComponent();
           
            //TimerStanbySreen
            this.Loaded += TimerForStandbyScreen;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            DataTemporary.idBuilding = int.Parse(e.Parameter as string); 
          
            VMCategory vmCategory = new VMCategory(DataTemporary.idBuilding);
            Gv_menukategori.DataContext = vmCategory;

        }

        private void Gv_menukategori_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var panel = (ItemsWrapGrid)Gv_menukategori.ItemsPanelRoot;
                panel.ItemWidth = e.NewSize.Width / 4;
                panel.ItemHeight = 230;
        }

        private void Gv_menukategori_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //TimerStanByScreen 

            DataTemporary.isStopRunAsyncForTask1 = true;
            DataTemporary.timer1 = 0;

            MCategory m = Gv_menukategori.SelectedItems[0] as MCategory;
            DataTemporary.imageCategory = m.originalPhoto;
            DataTemporary.category = m.title;

            Frame.Navigate(typeof(VDetailFloor360Degree), m.idCategory.ToString());
            Debug.WriteLine(m.idCategory);
           
        }

        private async void TimerForStandbyScreen(object sender, RoutedEventArgs e)
        {
            DataTemporary.isStopRunAsyncForTask1 = false;
            while (!DataTemporary.isStopRunAsyncForTask1)
            {
                DataTemporary.timer1++;
                await System.Threading.Tasks.Task.Delay(60000);
                Debug.WriteLine("Menu Utama  : " + DataTemporary.timer1);

                if (DataTemporary.timer1 > DataTemporary.timeLimit)
                {
                    DataTemporary.isStopRunAsyncForTask1 = true;
                    DataTemporary.timer1 = 0;
                    Frame.Navigate(typeof(StandbyScreen));
                }
            }
        }


        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            DataTemporary.isStopRunAsyncForTask1 = true;
            DataTemporary.timer1 = 0;
            Frame.Navigate(typeof(VGedungOption));
        }
    }
}
