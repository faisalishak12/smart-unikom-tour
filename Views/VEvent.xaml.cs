﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Unikom_Apps.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Unikom_Apps.Common;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using static Unikom_Apps.Models.HomeModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class VEvent : Page
    {
        public VEvent()
        {
            this.InitializeComponent();

            ProgressRing.IsActive = true;
            VMEvent vmevent = new VMEvent();
            //VMHome vmhome = new VMHome(); 
            gridEvent.DataContext = vmevent;
            //CarouselControl.DataContext = vmhome;

            LoadSlideShow(1);
        }

       // public object DataTemporary { get; private set; }

        private async void LoadSlideShow(int count)
        {
            int i = 0;
            bool isStopRunAsyncForTask4 = false;
            while (!isStopRunAsyncForTask4)
            {
                //CarouselControl.SelectedIndex = 9;                

                await System.Threading.Tasks.Task.Delay(2500);
                //CarouselControl.SelectedIndex = i;

                i++;

                if (i > count)
                    i = 0;

                ProgressRing.IsActive = DataTemporary.isActive;
            }
        }

        private void textFeatured_SelectionChanged(object sender, RoutedEventArgs e)
        {
            
        }

        private void Button_back_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Home));
        }

        private void gridEvent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
      
            //var panel = (ItemsWrapGrid)gridEvent.ItemsPanelRoot;


            /*if (DataTemporary.count <= 6)
            {
                panel.ItemWidth = e.NewSize.Width / 2;
                panel.ItemHeight = 350;
            }
            else
            {*/

            //panel.ItemWidth = e.NewSize.Width / 3;
            //panel.ItemHeight = 320;
        
        }

        private void gridEvent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Events data = (sender as GridView).SelectedItem as Events;
            Frame.Navigate(typeof(VDisplayEvent), data);
        }
    }
}
