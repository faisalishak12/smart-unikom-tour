﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Unikom_Apps.Common;
using Unikom_Apps.ViewModels;
using Unikom_Apps.Views;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using static Unikom_Apps.Models.HomeModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class VArtikel : Page
    {
        public VArtikel()
        {
            this.InitializeComponent();
            ProgressRing.IsActive = true;
            VMArticle vmartikel = new VMArticle();
            //gridArtikelAtas.DataContext = vmartikel;
            gridArtikelBawah.DataContext = vmartikel;

            this.Loaded += TimerForStandbyScreen;
        }

        private async void TimerForStandbyScreen(object sender, RoutedEventArgs e)
        {
            DataTemporary.isStopRunAsyncForTask4 = false;
            while (!DataTemporary.isStopRunAsyncForTask4)
            {
                await System.Threading.Tasks.Task.Delay(1000);
                ProgressRing.IsActive = DataTemporary.isActive;
            }
        }

        /*private void gridEvent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var panel = (ItemsWrapGrid)gridArtikelBawah.ItemsPanelRoot;
            /*if (DataTemporary.count <= 6)
            {
                panel.ItemWidth = e.NewSize.Width / 2;
                panel.ItemHeight = 350;
            }
            else
            {*/
            /*panel.ItemWidth = e.NewSize.Width / 3;
            panel.ItemHeight = 320;
        }*/



        private void gridArtikelAtas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void gridArtikelAtas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
           // var panel = (ItemsWrapGrid)gridArtikelAtas.ItemsPanelRoot;
            var width = ((Frame)Window.Current.Content).ActualWidth;
            /*if (DataTemporary.count <= 6)
            {
                panel.ItemWidth = e.NewSize.Width / 2;
                panel.ItemHeight = 350;
            }
            else
            {*/
            //panel.ItemWidth = e.NewSize.Width / 2;

            //panel.ItemHeight = 350;
        }


        private void Button_back_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Home));
        }

        private void gridArtikelBawah_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Artikel data = (sender as GridView).SelectedItem as Artikel;
            DataTemporary.idArticle = data.id_artikel;
            Frame.Navigate(typeof(VDisplayHtml), data);
        }

        private void Image_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var panel = (ItemsWrapGrid)gridArtikelBawah.ItemsPanelRoot;
            /*if (DataTemporary.count <= 6)
            {
                panel.ItemWidth = e.NewSize.Width / 2;
                panel.ItemHeight = 350;
            }
            else
            {*/
            

        }
    }
}
