﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Unikom_Apps.Common;
using Unikom_Apps.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Toolkit.Uwp.UI.Animations;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class VGedungOption : Page
    {
        public VGedungOption()
        {
            this.InitializeComponent();

            Panel_menu.Offset(
                offsetX: 0f,
                offsetY: -700f,
                duration: 400,
                delay: 0).StartAsync();

            RefreshCurrentTime();
            this.Loaded += TimerForStandbyScreen;

            //full screen
            var view = ApplicationView.GetForCurrentView();
            view.TryEnterFullScreenMode();
            view.FullScreenSystemOverlayMode = FullScreenSystemOverlayMode.Minimal;
            Colapsedtitlebar();
        }

        private async void TimerForStandbyScreen(object sender, RoutedEventArgs e)
        {
            DataTemporary.timer8 = 0;
            DataTemporary.isStopRunAsyncForTask8 = false;
            while (!DataTemporary.isStopRunAsyncForTask8)
            {
                DataTemporary.timer8++;
                await System.Threading.Tasks.Task.Delay(60000);
                Debug.WriteLine("VGedungOption : " + DataTemporary.timer8);
                if (DataTemporary.timer8 > DataTemporary.timeLimit)
                {
                    DataTemporary.isStopRunAsyncForTask8 = true;
                    DataTemporary.timer8 = 0;
                    Frame.Navigate(typeof(StandbyScreen));
                }
            }
        }

        private async void RefreshCurrentTime()
        {
            bool stopLooping = false;
            while (!stopLooping)
            {
                CurrentTime.Text = DateTime.Now.ToString("HH:mm:ss");
                await System.Threading.Tasks.Task.Delay(1000);
            }
        }

        private void webViewControl_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            // this.webViewControl.AddWebAllowedObject("sharedObj", communicationWinRT);
        }

        private void Colapsedtitlebar()
        {
            ApplicationViewTitleBar titlebar = Windows.UI.ViewManagement.ApplicationView.GetForCurrentView().TitleBar;
            titlebar.ButtonBackgroundColor = Windows.UI.Colors.White;
            titlebar.ButtonForegroundColor = Windows.UI.Colors.White;
            titlebar.ButtonHoverBackgroundColor = Windows.UI.Colors.White;
            titlebar.ButtonHoverForegroundColor = Windows.UI.Colors.White;
            titlebar.ButtonInactiveBackgroundColor = Windows.UI.Colors.White;
            titlebar.ButtonInactiveForegroundColor = Windows.UI.Colors.White;
            titlebar.ButtonPressedBackgroundColor = Windows.UI.Colors.White;
            titlebar.ButtonPressedForegroundColor = Windows.UI.Colors.White;
        }

        private void Button_menu_home(object sender, RoutedEventArgs e)
        {
            string tag = ((Button)sender).Tag.ToString();

            if (tag == "1")
            {
                DataTemporary.isStopRunAsyncForTask8 = true;
                DataTemporary.timer8 = 0;
                Frame.Navigate(typeof(MenuUtama), "1");
            }
            else if (tag == "2")
            {
                DataTemporary.isStopRunAsyncForTask8 = true;
                DataTemporary.timer8 = 0;
                Frame.Navigate(typeof(MenuUtama), "2");
            }
            else if (tag == "3")
            {
                DataTemporary.isStopRunAsyncForTask8 = true;
                DataTemporary.timer8 = 0;
                DataTemporary.category = "unikom dago";
                Frame.Navigate(typeof(VDetailFloor360Degree), "17");
            }

        }

        private async void Back_home_Click(object sender, RoutedEventArgs e)
        {
            //end thread
            DataTemporary.isStopRunAsyncForTask8 = true;
            DataTemporary.timer8 = 0;

            await Panel_menu.Offset(
                offsetX: 0f,
                offsetY: 700f,
                duration: 300,
                delay: 0).StartAsync();

            Frame.Navigate(typeof(Home));
        }


        private async void About_Button_Click(object sender, RoutedEventArgs e)
        {
            DataTemporary.timer8 = 0;
            ContentDialogAbout dialog = new ContentDialogAbout();
            await dialog.ShowAsync();
        }

        private void CurrentTime_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(StandbyScreen));
        }
    }
}

