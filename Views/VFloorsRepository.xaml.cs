﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Unikom_Apps.Models;
using Unikom_Apps.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.Toolkit.Uwp.UI.Animations;
using Unikom_Apps.Common;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class VFloorsRepository : Page
    {

        public VFloorsRepository()
        {
            this.InitializeComponent();

            VMFloors vmfloor = new VMFloors();
            Lv_Floor.DataContext = vmfloor;

            //Time for StandbyScreen
            this.Loaded += TimerForStandbyScreen;
        }

        private async void TimerForStandbyScreen(object sender, RoutedEventArgs e)
        {
            DataTemporary.timer5 = 0;
            DataTemporary.isStopRunAsyncForTask3 = false;
            while (!DataTemporary.isStopRunAsyncForTask5)
            {
                await System.Threading.Tasks.Task.Delay(60000);
                DataTemporary.timer5++;
                Debug.WriteLine("VFloorRepository : " + DataTemporary.timer5);
                if (DataTemporary.timer5 > DataTemporary.timeLimit)
                {
                    DataTemporary.isStopRunAsyncForTask5 = true;
                    Frame.Navigate(typeof(StandbyScreen));
                    DataTemporary.timer5 = 0;
                }
            }
        }

        private void Button_back_Click(object sender, RoutedEventArgs e)
        {
            DataTemporary.isStopRunAsyncForTask5 = true;
            DataTemporary.timer5 = 0;
            Frame.Navigate(typeof(Home));
        }
    }
}
