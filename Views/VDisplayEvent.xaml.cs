﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Unikom_Apps.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using static Unikom_Apps.Models.HomeModel;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class VDisplayEvent : Page
    {
        public VDisplayEvent()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Events obj = e.Parameter as Events;
            
            //Navigation to WebView
            base.OnNavigatedTo(e);
            //this.webViewControl.Navigate(new Uri(src));
            VMDisplayEvent vm = new VMDisplayEvent(obj);

            this.DataContext = vm;
        }

        private void Button_back_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(VEvent));
        }
    }
}
