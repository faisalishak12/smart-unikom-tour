﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Unikom_Apps.Common;
using Unikom_Apps.Models;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class V360Degree : Page
    {
        //private SharedObject communicationWinRT = new SharedObject();

        public V360Degree()
        {
            this.InitializeComponent();

            //TimerStanbySreen
            this.Loaded += TimerForStandbyScreen;

        }

        private async void TimerForStandbyScreen(object sender, RoutedEventArgs e)
        {
            DataTemporary.isStopRunAsyncForTask2 = false;
            while (!DataTemporary.isStopRunAsyncForTask2)
            {
                DataTemporary.timer2++;
                await System.Threading.Tasks.Task.Delay(80000);
                Debug.WriteLine( "V360Degree :" + DataTemporary.timer2);
                if (DataTemporary.timer2 > DataTemporary.timeLimit)
                {
                    DataTemporary.isStopRunAsyncForTask2 = true;
                    DataTemporary.timer2 = 0;
                    Frame.Navigate(typeof(StandbyScreen));
                }
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            //Navigation to WebView
            M360Degree data = e.Parameter as M360Degree;

            string src;

           // ms - appx - web:///Html/
            if (data.url != null)
            {
                Debug.WriteLine("url-avaliable" + data.url);
                src = "ms-appx-web:///Html/" + data.url;
            }else {
                Debug.WriteLine("url-not-avaliable");
                src = "ms-appx-web:///Html/360.html?gambar=" + data.originalImagename + "&pitch=" + data.pitch + "&yaw=" + data.yaw + "";
            }
            this.webViewControl.Navigate(new Uri(src));

            Debug.WriteLine("Gambar : " + data.originalImagename);
        }

       /*private void webViewControl_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            this.webViewControl.AddWebAllowedObject("sharedObj", communicationWinRT);
        }*/

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            //end task
            DataTemporary.timer2 = 0;
            DataTemporary.isStopRunAsyncForTask2 = true;

            Frame.Navigate(typeof(VDetailFloor360Degree), DataTemporary.idCategory);
        }

        private void webViewControl_Tapped(object sender, TappedRoutedEventArgs e)
        {
            DataTemporary.timer2 = 0;
        }

    }
}
