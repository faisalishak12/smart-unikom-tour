﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Unikom_Apps.Common;
using Unikom_Apps.ViewModels;
using Unikom_Apps.Views;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Microsoft.Toolkit.Uwp.UI.Animations;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Collections.ObjectModel;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Unikom_Apps.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Home : Page
    {
        //private Windows.System.Display.DisplayRequest _displayRequest;
        //private short _requestCount;

        public Home()
        {
            this.InitializeComponent();

            //Hook ViewModel
            VM360Degree vm360degree = new VM360Degree(null);
            Rotator_tile1.DataContext = vm360degree;
            VMEvent vmEvent = new VMEvent();
            Rotator_tile2.DataContext = vmEvent;
            VMArticle vmArticle = new VMArticle();
            Rotator_tile3.DataContext = vmArticle;
            VM360Degree vm360degreeAsc = new VM360Degree("asc");
            /*Rotator_tile2.DataContext = vm360degreeAsc;
            VM360Degree vm360degreeDesc = new VM360Degree("desc");
            Rotator_tile3.DataContext = vm360degreeDesc*/

            

            //menu trantition
            Panel_menu.Offset(
                offsetX: 0f,
                offsetY: -700f,
                duration: 400,
                delay: 0).StartAsync();

            this.Loaded += TimerForStandbyScreen;
            RefreshCurrentTime();   

            //full screen
            var view = ApplicationView.GetForCurrentView();
            view.TryEnterFullScreenMode();
            view.FullScreenSystemOverlayMode = FullScreenSystemOverlayMode.Minimal;
            Colapsedtitlebar();
        }

        private async void RefreshCurrentTime()
        {
            bool stopLooping = false;
            while (!stopLooping)
            {
                CurrentTime.Text = DateTime.Now.ToString("HH:mm:ss");
                await System.Threading.Tasks.Task.Delay(1000);
            }
        }

        private async void TimerForStandbyScreen(object sender, RoutedEventArgs e)
        {
            DataTemporary.timer3 = 0;
            DataTemporary.isStopRunAsyncForTask3 = false;
            while (!DataTemporary.isStopRunAsyncForTask3)
            {
                DataTemporary.timer3++;
                await System.Threading.Tasks.Task.Delay(60000);
                Debug.WriteLine("Home : " + DataTemporary.timer3);
                if (DataTemporary.timer3 > DataTemporary.timeLimit)
                {
                    DataTemporary.isStopRunAsyncForTask3 = true;
                    DataTemporary.timer3 = 0;                    
                    Frame.Navigate(typeof(StandbyScreen));
                }
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Navigation to WebView
            base.OnNavigatedTo(e);
            //this.webViewControl.Navigate(new Uri(src));
        }

        private void webViewControl_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
           // this.webViewControl.AddWebAllowedObject("sharedObj", communicationWinRT);
        }

        private void Colapsedtitlebar()
        {
            ApplicationViewTitleBar titlebar = Windows.UI.ViewManagement.ApplicationView.GetForCurrentView().TitleBar;
            titlebar.ButtonBackgroundColor = Windows.UI.Colors.White;
            titlebar.ButtonForegroundColor = Windows.UI.Colors.White;
            titlebar.ButtonHoverBackgroundColor = Windows.UI.Colors.White;
            titlebar.ButtonHoverForegroundColor = Windows.UI.Colors.White;
            titlebar.ButtonInactiveBackgroundColor = Windows.UI.Colors.White;
            titlebar.ButtonInactiveForegroundColor = Windows.UI.Colors.White;
            titlebar.ButtonPressedBackgroundColor = Windows.UI.Colors.White;
            titlebar.ButtonPressedForegroundColor = Windows.UI.Colors.White;
        }

        private async void Button_menu_home(object sender, RoutedEventArgs e)
        {
            string tag = ((Button)sender).Tag.ToString();

            if (tag == "1")
            {
                DataTemporary.isStopRunAsyncForTask3 = true;
                DataTemporary.timer3 = 0;
                await Panel_menu.Offset(
                    offsetX: 0f,
                    offsetY: 700f,
                    duration: 300,
                    delay: 0).StartAsync();

                Frame.Navigate(typeof(VGedungOption));
            }
            else if (tag == "2")
            {
                DataTemporary.isStopRunAsyncForTask3 = true;
                DataTemporary.timer3 = 0;
                Frame.Navigate(typeof(VFloorsRepository));
            }
            else if (tag == "3")
            {
                DataTemporary.isStopRunAsyncForTask3 = true;
                DataTemporary.timer3 = 0;
                Frame.Navigate(typeof(VGetUnikomApps));
            }
            else if (tag == "4")
            {
                DataTemporary.isStopRunAsyncForTask3 = true;
                DataTemporary.timer3 = 0;
                Frame.Navigate(typeof(VEvent));
            }
            else if (tag == "5")
            {
                DataTemporary.isStopRunAsyncForTask3 = true;
                DataTemporary.timer3 = 0;
                Frame.Navigate(typeof(VArtikel));
            }
            else if (tag == "6")
            {
                DataTemporary.isStopRunAsyncForTask3 = true;
                DataTemporary.timer3 = 0;

                var result = await VMCalendar.LoadURL();
                ObservableCollection<VMCalendar> collection = new ObservableCollection<VMCalendar>(result);

                Frame.Navigate(typeof(VCalendar), collection);
            }
        }

        private async void About_Button_Click(object sender, RoutedEventArgs e)
        {
            DataTemporary.timer3 = 0;
            ContentDialogAbout dialog = new ContentDialogAbout();
            await dialog.ShowAsync();
        }

        private void CurrentTime_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(StandbyScreen));
        }
    }
}
