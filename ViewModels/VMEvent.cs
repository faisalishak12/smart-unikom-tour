﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Unikom_Apps.Common;
using static Unikom_Apps.Models.HomeModel;

namespace Unikom_Apps.ViewModels
{
    class VMEvent : ViewModelBase
    {
        private static int sizeCollection;
        public static int SizeCollection {
            get
            {
                return sizeCollection;
            }set {
                sizeCollection = value;
            }

        }

        private ObservableCollection<Events> collectionevents = new ObservableCollection<Events>();
        public ObservableCollection<Events> CollectionEvents
        {
            get
            {
                return collectionevents;
            }
            set
            {
                if (this.collectionevents != value)
                {
                    collectionevents = value;
                    RaisePropertyChanged("");
                }
            }
        }

        private ObservableCollection<Artikel> collectionartikel = new ObservableCollection<Artikel>();
        public ObservableCollection<Artikel> CollectionArtikel
        {
            get
            {
                return collectionartikel;
            }
            set
            {
                if (this.collectionartikel != value)
                {
                    collectionartikel = value;
                    RaisePropertyChanged("");
                }
            }
        }

        public VMEvent()
        {
            LoadURL();
        }

        private async void LoadURL()
        {
            DataTemporary.isActive = true;
            HttpClient clienthome = new HttpClient();
            try
            {
                String RespondResult = await clienthome.GetStringAsync(new Uri("http://www.unikom-app.net/api/v1/event"));
                DownloadListHome(RespondResult);
                DataTemporary.isActive = false;
            }
            catch (Exception e)
            {
                DataTemporary.isActive = false;
                Debug.Write(e);
            }

        }

        private void DownloadListHome(string respondResult)
        {
            JObject jresult = JObject.Parse(respondResult);
            JArray listEvent = JArray.Parse(jresult.SelectToken("result").ToString());
            DataTemporary.SizeColectionEvent = listEvent.Count;
            foreach (JObject item in listEvent)
            {
                Events mevents = new Events();
                mevents.id_event = Int32.Parse(item.SelectToken("id_event").ToString());
                mevents.nama_event = item.SelectToken("nama_event").ToString();
                mevents.deskripsi_event =  "<style>*{font-family:Segoe UI,calibri}</style>" + item.SelectToken("deskripsi_event").ToString();
                mevents.pengirim = item.SelectToken("pengirim").ToString();
                mevents.kategori_event = item.SelectToken("kategori_event").ToString();
                mevents.foto = URL.urlBase + "img_event/" + item.SelectToken("foto").ToString();
                mevents.tumbnail = URL.urlBase + "img_event/" + item.SelectToken("foto").ToString();
                mevents.slug = item.SelectToken("slug").ToString();
                mevents.penyelenggara = item.SelectToken("penyelenggara").ToString();
                mevents.tgl_event = item.SelectToken("tgl_event").ToString();
                mevents.lokasi_event = item.SelectToken("lokasi_event").ToString();
                mevents.fotoPenyelenggara = "ms-appx:///images/Organization/" + item.SelectToken("penyelenggara").ToString() + ".jpg";
                collectionevents.Add(mevents);
            }

            sizeCollection = collectionevents.Count();
        }
    }
}
