﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Unikom_Apps.Common;
using Unikom_Apps.Models;

namespace Unikom_Apps.ViewModels
{
    class VMCalendar 
    {
        /*#region Properties
        public string food_name { get; set; }
        public string category { get; set; }

        #endregion

        public VMCalendar() {
            food_name = string.Empty;
            category = string.Empty;
        }


        #region Public Methods
        public static VMCalendar GetCalendar(string _food_name, string _category)
        {
            return new VMCalendar()
            {
                food_name = _food_name,
                category = _category,
            };
        }

        public static async Task<ObservableCollection<ListGroup>> GetCalendarGrouped() {
            ObservableCollection<ListGroup> groups = new ObservableCollection<ListGroup>();
            ObservableCollection<VMCalendar> vm;
            vm = await LoadURL();

            var query = from item in vm
                        group item by item.category into g
                        select new { GroupName = g.Key, Items = g };
            Debug.WriteLine(vm.Count);

            foreach (var g in query)
            {
                ListGroup info = new ListGroup();
                info.Key = g.GroupName;
                foreach (var item in g.Items)
                {
                    info.Add(item);
                }
                groups.Add(info);
            }

            return groups;
        }


        private static async Task<ObservableCollection<VMCalendar>> LoadURL()
        {
            ObservableCollection<VMCalendar> collectionCalendar = new ObservableCollection<VMCalendar>();
            DataTemporary.isActive = true;
            HttpClient clienthome = new HttpClient();
            try
            {
                String RespondResult = await clienthome.GetStringAsync(new Uri("http://localhost:8000/api/food"));
                DataTemporary.isActive = false;
                Debug.Write(RespondResult);

                JObject jresult = JObject.Parse(RespondResult);
                JArray listCalendar = JArray.Parse(jresult.SelectToken("result").ToString());

                foreach (JObject item in listCalendar)
                {
                    VMCalendar m = new VMCalendar();

                    m.food_name = item.SelectToken("food_name").ToString();
                    m.category = item.SelectToken("category").ToString();
                    GetCalendar(item.SelectToken("food_name").ToString(), item.SelectToken("category").ToString());
                    Debug.WriteLine(item.SelectToken("category").ToString());

                    collectionCalendar.Add(m);
                }
            }
            catch (Exception e)
            {
                DataTemporary.isActive = false;
            }

            return collectionCalendar;
        }
        #endregion
        */

        #region Properties
        public static ObservableCollection<VMCalendar> collectionCalendar = new ObservableCollection<VMCalendar>();
        public string food_name { get; set; }
        public string category { get; set; }
       
        #endregion

        public VMCalendar()
        {
            food_name = string.Empty;
            category = string.Empty;
        }

        #region Public Methods
        public static VMCalendar GetNewContact(string a)
        {
            return new VMCalendar()
            {
                food_name = a,
                category = a,
            };
        }

        private static ObservableCollection<VMCalendar> GetCollectionCalendar(ObservableCollection<VMCalendar> request)
        {
            ObservableCollection<VMCalendar> collection = new ObservableCollection<VMCalendar>();

            foreach (var item in request)
            {
                collection.Add(GetNewContact(item.food_name));
            }

            return collection;

        }

        public static ObservableCollection<ListGroup> GetContactsGrouped(ObservableCollection<VMCalendar> request)
        {
            ObservableCollection<ListGroup> groups = new ObservableCollection<ListGroup>();

            var query = from item in GetCollectionCalendar(request)
                        group item by item.category into g
                        orderby g.Key
                        select new { GroupName = g.Key, Items = g };

            foreach (var g in query)
            {
                ListGroup info = new ListGroup();
                info.Key = g.GroupName;
                foreach (var item in g.Items)
                {
                    info.Add(item);
                }
                groups.Add(info);
            }

            return groups;
        }

        /* public static VMCalendar GetNewContact(string _food_name, string _category)
         {
             return new VMCalendar()
             {
                 food_name = _food_name,
                 category = _category,
             };
         }*/

        public static async Task<ObservableCollection<VMCalendar>> LoadURL()
        {
            ObservableCollection<VMCalendar> collectionCalendars = new ObservableCollection<VMCalendar>();
            DataTemporary.isActive = true;
            HttpClient clienthome = new HttpClient();
            try
            {
                String RespondResult = await clienthome.GetStringAsync(new Uri("http://localhost:8000/api/food"));
                DataTemporary.isActive = false;

                JObject jresult = JObject.Parse(RespondResult);
                JArray listCalendar = JArray.Parse(jresult.SelectToken("result").ToString());

                foreach (JObject item in listCalendar)
                {
                    VMCalendar m = new VMCalendar();
                    m.food_name = item.SelectToken("food_name").ToString();
                    m.food_name = item.SelectToken("category").ToString();
                    collectionCalendars.Add(m);
                }
            }
            catch (Exception e)
            {
                DataTemporary.isActive = false;
            }

            return collectionCalendars;
        }
        #endregion

    }
}
