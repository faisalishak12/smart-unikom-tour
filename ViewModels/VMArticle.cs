﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Unikom_Apps.Common;
using static Unikom_Apps.Models.HomeModel;

namespace Unikom_Apps.ViewModels
{
    class VMArticle : ViewModelBase
    {
        private ObservableCollection<Artikel> collectionartikel = new ObservableCollection<Artikel>();
        private ObservableCollection<Artikel> collectionfeatured = new ObservableCollection<Artikel>();
        public ObservableCollection<Artikel> CollectionArtikel
        {
            get
            {
                return collectionartikel;
            }
            set
            {
                if (this.collectionartikel != value)
                {
                    collectionartikel = value;
                    RaisePropertyChanged("");
                }
            }
        }

        public ObservableCollection<Artikel> CollectionFeatured
        {
            get
            {
                return collectionfeatured;
            }
            set
            {
                if (this.collectionfeatured != value)
                {
                    collectionfeatured = value;
                    RaisePropertyChanged("");
                }
            }
        }

        public VMArticle()
        {
            LoadURL();
        }

        private async void LoadURL()
        {
            DataTemporary.isActive = true;
            HttpClient clienthome = new HttpClient();
            try
            {
                String RespondResult = await clienthome.GetStringAsync(new Uri("http://www.unikom-app.net/api/v1/artikel"));
                DoanloadData(RespondResult);
                DataTemporary.isActive = false;
            }
            catch (Exception e)
            {
                Debug.Write(e);
                DataTemporary.isActive = false;
            }

        }

        private void DoanloadData(string respondResult)
        {
            JObject jresult = JObject.Parse(respondResult);
            JArray listArtikel = JArray.Parse(jresult.SelectToken("result").ToString());
            int i = 1;
            foreach (JObject item in listArtikel)
            {

                Artikel martikel = new Artikel();
                martikel.id_artikel = Int32.Parse(item.SelectToken("id_artikel").ToString());
                martikel.judul_artikel = item.SelectToken("judul_artikel").ToString();
                martikel.kategori_artikel = item.SelectToken("kategori_artikel").ToString();
                martikel.foto = "http://unikom-app.net/img_artikel/" + item.SelectToken("foto").ToString();
                martikel.tumbnail = "http://unikom-app.net/img_artikel/" + item.SelectToken("foto").ToString();
                martikel.tgl_post = item.SelectToken("tgl_post").ToString();
                martikel.post = item.SelectToken("post").ToString();
                martikel.pengirim = item.SelectToken("pengirim").ToString();
                martikel.slug = item.SelectToken("slug").ToString();
                martikel.fotoPengirim = "ms-appx:///images/Organization/"+ item.SelectToken("pengirim").ToString() +".jpg";
                if (i < 3)
                {
                    collectionfeatured.Add(martikel);
                }
                else
                {
                    collectionartikel.Add(martikel);
                }
                i++;
            }
        }
    }
}
