﻿

using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using Unikom_Apps.Common;
using Unikom_Apps.Models;

namespace Unikom_Apps.ViewModels
{
    class VMFloors : ViewModelBase
    {
        string path;
        SQLite.Net.SQLiteConnection conn;

        private ObservableCollection<MFloors> _collection = new ObservableCollection<MFloors>();
        public ObservableCollection<MFloors> collection
        {
            get { return _collection; }
            set
            {
                if (this._collection != value)
                {
                    _collection = value;
                    RaisePropertyChanged("");
                }
            }
        }

        public VMFloors()
        {
            path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "db.sqlite");
            Debug.WriteLine(path);
            conn = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);
            conn.CreateTable<MFloors>();
            Debug.WriteLine(path);

            this.LoadItem();
        }

        private void LoadItem()
        {
            var query = conn.Table<MFloors>();
            foreach (MFloors item in query)
            {
                MFloors floor = new MFloors();
                floor.idFloor = item.idFloor;
                floor.title = item.title;
                floor.description = item.description;

                _collection.Add(floor);
            }
        }
    }
}
