﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Unikom_Apps.Common;
using Unikom_Apps.Models;
using static Unikom_Apps.Models.HomeModel;

namespace Unikom_Apps.ViewModels
{
    class VNDisplayArticle : ViewModelBase
    {
        private Artikel m = new Artikel();


        public Artikel _m
        {
            get { return m; }
            set { m = value; }
        }

        public String Post
        {
            get { return _m.post; }
            set
            {
                _m.post = value;
                RaisePropertyChanged("");
            }
        }


        public String Foto
        {
            get { return _m.foto; }
            set
            {
                _m.foto = value;
                RaisePropertyChanged("");
            }
        }

        public String Judul_artikel
        {
            get { return _m.judul_artikel; }
            set
            {
                _m.judul_artikel = value;
                RaisePropertyChanged("");
            }
        }

        public String Tgl_post
        {
            get { return _m.tgl_post; }
            set
            {
                _m.tgl_post = value;
                RaisePropertyChanged("");
            }
        }

        public String Pengirim
        {
            get { return _m.pengirim; }
            set
            {
                _m.pengirim = value;
                RaisePropertyChanged("");
            }
        }


        public VNDisplayArticle(Artikel obj)
        {
            Post = obj.post;
            Foto =  obj.foto;
            Tgl_post = obj.tgl_post;
            Pengirim = obj.pengirim;
            Judul_artikel = obj.judul_artikel;
            //this.LoadURL();
        }

    }
}
