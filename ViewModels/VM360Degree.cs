﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unikom_Apps.Common;
using Unikom_Apps.Models;

namespace Unikom_Apps.ViewModels
{
    class VM360Degree : ViewModelBase
    {
        string path;
        SQLite.Net.SQLiteConnection conn;

        private ObservableCollection<M360Degree> _collection = new ObservableCollection<M360Degree>();
        public ObservableCollection<M360Degree> collection
        {
            get { return _collection; }
            set
            {
                if (this._collection != value)
                {
                    _collection = value;
                    RaisePropertyChanged("");
                }
            }
        }

        public VM360Degree(string str)
        {
            path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "db.sqlite");
            Debug.WriteLine("Path SQLITE : " + path);
            conn = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);
            conn.CreateTable<M360Degree>();

            this.LoadItem(str);
        }

        private void LoadItem(string str)
        {
            var query = conn.Table<M360Degree>();

            if (str == "desc")
            {
                query = conn.Table<M360Degree>().OrderByDescending(c => c.imageName);
            }
            else if (str == "asc")
            {
                query = conn.Table<M360Degree>().OrderBy(c => c.imageName);
            }

            int count = 0;
            foreach (M360Degree item in query)
            {
                if (str == item.idCategory || str == null || str == "desc" || str == "asc") { 
                    M360Degree m = new M360Degree();
                    m.idImage = item.idImage;
                    m.imageName = URL.photo360 + item.imageName;
                    m.tumbnail = URL.tumbnail + item.tumbnail;
                    m.title = item.title;
                    m.location = "Located at " + item.location;
                    m.originalImagename = item.imageName;
                    m.pitch = item.pitch;
                    m.yaw = item.yaw;
                    m.url = item.url;
                    _collection.Add(m);
                    count++;
                }
            }
            DataTemporary.count = count;
        }
    }
}
