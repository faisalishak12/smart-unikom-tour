﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Unikom_Apps.Common;
using static Unikom_Apps.Models.HomeModel;

namespace Unikom_Apps.ViewModels
{
    class VMDisplayEvent : ViewModelBase
    {
        private Events m = new Events();


        public Events _m
        {
            get { return m; }
            set { m = value; }
        }

        public String DeskripsiEvent
        {
            get { return _m.deskripsi_event; }
            set
            {
                _m.deskripsi_event = value;
                RaisePropertyChanged("");
            }
        }

        public String Foto
        {
            get { return _m.foto; }
            set
            {
                _m.foto = value;
                RaisePropertyChanged("");
            }
        }

        public String NamaEvent
        {
            get { return _m.nama_event; }
            set
            {
                _m.nama_event = value;
                RaisePropertyChanged("");
            }
        }

        public String Penyelenggara
        {
            get { return _m.penyelenggara; }
            set
            {
                _m.penyelenggara = value;
                RaisePropertyChanged("");
            }
        }

        public String TglEvent
        {
            get { return _m.tgl_event; }
            set
            {
                _m.tgl_event = value;
                RaisePropertyChanged("");
            }
        }


        public String LokasiEvent
        {
            get { return _m.lokasi_event; }
            set
            {
                _m.lokasi_event = value;
                RaisePropertyChanged("");
            }
        }

        public VMDisplayEvent(Events obj)
        {
            DeskripsiEvent = obj.deskripsi_event;
            Foto = obj.foto;
            NamaEvent = obj.nama_event;
            TglEvent = obj.tgl_event;
            LokasiEvent = obj.lokasi_event;
            Penyelenggara = obj.penyelenggara;
        }

       /* private async void LoadURL()
        {
            /* Artikel martikel = new Artikel();
            martikel.id_artikel = Int32.Parse(item.SelectToken("id_artikel").ToString());
            martikel.judul_artikel = item.SelectToken("judul_artikel").ToString();
            martikel.kategori_artikel = item.SelectToken("kategori_artikel").ToString();
            martikel.foto = URL.urlBase + "img_artikel/" + item.SelectToken("foto").ToString();
            martikel.tumbnail = URL.urlBase + "img_artikel/" + item.SelectToken("foto").ToString();
            martikel.tgl_post = item.SelectToken("tgl_post").ToString();*/
            //Post = .SelectToken("post").ToString();
            /*martikel.pengirim = item.SelectToken("pengirim").ToString();
            martikel.slug = item.SelectToken("slug").ToString();*/

        //}
    }
}
