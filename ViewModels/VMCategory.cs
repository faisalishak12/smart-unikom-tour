﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unikom_Apps.Common;
using Unikom_Apps.Models;

namespace Unikom_Apps.ViewModels
{
    class VMCategory : ViewModelBase
    {
        string path;
        SQLite.Net.SQLiteConnection conn;

        private ObservableCollection<MCategory> _collection = new ObservableCollection<MCategory>();
        public ObservableCollection<MCategory> collection
        {
            get { return _collection; }
            set
            {
                if (this._collection != value)
                {
                    _collection = value;
                    RaisePropertyChanged("");
                }
            }
        }

        public VMCategory(int idBuilding)
        {
            path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "db.sqlite");
            Debug.WriteLine(path);
            conn = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);
            conn.CreateTable<MCategory>();
            Debug.WriteLine(path);

            this.LoadItem(idBuilding);
        }

        private void LoadItem(int idBUilding)
        {
            var query = conn.Table<MCategory>().Where(c => c.idBuilding == idBUilding);
            foreach (MCategory item in query)
            {
                MCategory floor = new MCategory();
                floor.idCategory = item.idCategory;
                floor.title = item.title;
                floor.originalPhoto = URL.tumbnail + item.originalPhoto;

                _collection.Add(floor);
            }
        }
    }
}
