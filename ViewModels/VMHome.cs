﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Unikom_Apps.Common;
using static Unikom_Apps.Models.HomeModel;

namespace Unikom_Apps.ViewModels
{
    class VMHome : ViewModelBase
    {
        private ObservableCollection<Events> collectionevents = new ObservableCollection<Events>();
        public ObservableCollection<Events> CollectionEvents
        {
            get
            {
                return collectionevents;
            }
            set
            {
                if (this.collectionevents != value)
                {
                    collectionevents = value;
                    RaisePropertyChanged("");
                }
            }
        }

        private ObservableCollection<Artikel> collectionartikel = new ObservableCollection<Artikel>();
        public ObservableCollection<Artikel> CollectionArtikel
        {
            get
            {
                return collectionartikel;
            }
            set
            {
                if (this.collectionartikel != value)
                {
                    collectionartikel = value;
                    RaisePropertyChanged("");
                }
            }
        }

        public VMHome()
        {
            DataTemporary.isNull = false;
            LoadURL();
        }

        private async void LoadURL()
        {
            HttpClient clienthome = new HttpClient();
            try
            {
                String RespondResult = await clienthome.GetStringAsync(new Uri("http://www.unikom-app.net/api/v1/home"));
                DownloadListHome(RespondResult);
            }
            catch 
            {
                Debug.Write("haha");
                DataTemporary.isNull = true;
            }

        }

        private void DownloadListHome(string respondResult)
        {
                JObject jresult = JObject.Parse(respondResult);
                JObject result = JObject.Parse(jresult.SelectToken("result").ToString());
                JArray listEvent = JArray.Parse(result.SelectToken("event").ToString());
                JArray listArtikel = JArray.Parse(result.SelectToken("artikel").ToString());

                foreach (JObject item in listEvent)
                {
                    Events mevents = new Events();
                    mevents.id_event = Int32.Parse(item.SelectToken("id_event").ToString());
                    mevents.nama_event = item.SelectToken("nama_event").ToString();
                    mevents.deskripsi_event = item.SelectToken("deskripsi_event").ToString();
                    mevents.pengirim = item.SelectToken("pengirim").ToString();
                    mevents.kategori_event = item.SelectToken("kategori_event").ToString();
                    mevents.foto = URL.urlBase + "img_event/" + item.SelectToken("foto").ToString();
                    mevents.slug = item.SelectToken("slug").ToString();
                    mevents.penyelenggara = item.SelectToken("penyelenggara").ToString();
                    mevents.tgl_event = item.SelectToken("tgl_event").ToString();
                    mevents.lokasi_event = item.SelectToken("lokasi_event").ToString();
                    collectionevents.Add(mevents);
                }

                foreach (JObject item in listArtikel)
                {
                    Artikel martikel = new Artikel();
                    martikel.id_artikel = Int32.Parse(item.SelectToken("id_artikel").ToString());
                    martikel.judul_artikel = item.SelectToken("judul_artikel").ToString();
                    martikel.kategori_artikel = item.SelectToken("kategori_artikel").ToString();
                    martikel.foto = URL.urlBase +"img_artikel/"+ item.SelectToken("foto").ToString();
                    martikel.tgl_post = item.SelectToken("tgl_post").ToString();
                    martikel.post = item.SelectToken("post").ToString();
                    martikel.pengirim = item.SelectToken("pengirim").ToString();
                    martikel.slug = item.SelectToken("slug").ToString();
                    collectionartikel.Add(martikel);
                }

            if (collectionevents == null)
            {
                DataTemporary.isNull = true;
            }



        }
    }
}
