﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unikom_Apps.Models
{
    class MFloors
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idFloor { get; set; }
        public string title { get; set; }
        public string description { get; set; }
    }
}
