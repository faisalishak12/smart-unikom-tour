﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unikom_Apps.Models
{
    class MCategory
    {
        [PrimaryKey][AutoIncrement]
        public int idCategory { get; set; }
        public string title { get; set; }
        public string originalPhoto { get; set; }
        public int idBuilding { get; set; }

    }
}
