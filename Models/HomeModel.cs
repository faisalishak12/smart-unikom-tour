﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unikom_Apps.Models
{
    class HomeModel
    {
        public class Events
        {
            public int id_event { get; set; }
            public string nama_event { get; set; }
            public string deskripsi_event { get; set; }
            public string kategori_event { get; set; }
            public string foto { get; set; }
            public string tgl_event { get; set; }
            public string lokasi_event { get; set; }
            public string slug { get; set; }
            public string pengirim { get; set; }
            public string penyelenggara { get; set; }
            public string tumbnail { get; set; }
            public string fotoPenyelenggara { get; set; }
        }

        public class Artikel
        {
            public int id_artikel { get; set; }
            public string judul_artikel { get; set; }
            public string kategori_artikel { get; set; }
            public string foto { get; set; }
            public string tgl_post { get; set; }
            public string post { get; set; }
            public string slug { get; set; }
            public string pengirim { get; set; }
            public string tumbnail { get; set; }
            public string fotoPengirim { get; set; }
        }
    }
}
