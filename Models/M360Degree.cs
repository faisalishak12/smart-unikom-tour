﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unikom_Apps.Models
{
    class M360Degree
    {
        [PrimaryKey]
        [AutoIncrement]
        public int idImage { get; set; }
        public string imageName { get; set; }
        public string title { get; set; }
        public string location { get; set; }
        public string originalImagename { get; set; }
        public string tumbnail { get; set; }
        public string idCategory { get; set; }
        public string pitch { get; set; }
        public string yaw { get; set; }
        public string url { get; set; }
    }
}
